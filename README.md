# kohamera
A simple web app camera software for taking ID photos

FEATURES
===========
* Simple user interface
* Static capture size (adjustable in source)
* Dynamic capture area by moving mouse

ROADMAP
===========
1. Initial release
2. Code optimization
3. Clear results area when result is downloaded
3. Optional feature - dynamic capture size on page
4. Optional feature - Results area hidden until capture is taken
5. Optional feature - scale capture area to window size
